package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private static final Random RANDOM = new Random();
    private Canvas canvas = new Canvas();
    private int greenX = -1, greenY = -1;
    //red stones
    private ArrayList<Integer> stonesX = new ArrayList<>();
    private ArrayList<Integer> stonesY = new ArrayList<>();
    //snake
    private ArrayList<Integer> snakeX = new ArrayList<>();
    private ArrayList<Integer> snakeY = new ArrayList<>();

    private int keyCode;
    private boolean gameover = false;
    
    public static void main(String[] args) {
        new Program().go();
    }

    public Program() {
        //create the initial object & set the starting position
        for (int i = 0; i < 6; i++) {
            snakeX.add(10 - i);
            snakeY.add(10);
        }

        //set the environment
        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        canvas.setBackground(Color.white);
        add(BorderLayout.CENTER, canvas);
        setVisible(true);

        //add keyboard event
        this.keyCode = 39;
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int keyBoardCode = e.getKeyCode();
                if ((keyBoardCode >= 37) && (keyBoardCode <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.keyCode - keyBoardCode) != 2) {// block moving back
                        Program.this.keyCode = keyBoardCode;
                    }
                }
            }
        });
    }

    void go() { // main loop
        while (!gameover) {
            int headX = snakeX.get(0);
            int headY = snakeY.get(0);
            if (keyCode == 37) {
                headX--;
            }
            if (keyCode == 39) {
                headX++;
            }
            if (keyCode == 38) {
                headY--;
            }
            if (keyCode == 40) {
                headY++;
            }
            //if reach the right border, start from the left
            if (headX > 30 - 1) {
                headX = 0;
            }
            //if reach the left border, start from the right
            if (headX < 0) {
                headX = 30 - 1;
            }
            //if reach the bottom border, start from the top
            if (headY > 20 - 1) {
                headY = 0;
            }
            //if reach the top border, start from the bottom
            if (headY < 0) {
                headY = 20 - 1;
            }

            //if touch the stones
            boolean isTouchStone = false;
            for (int i = 0; i < stonesX.size(); i++) {
                if (stonesX.get(i) == headX && stonesY.get(i) == headY) {
                    isTouchStone = true;
                }
            }

            //if touch itself
            boolean isTouchItself = false;
            for (int i1 = 0; i1 < snakeX.size(); i1++) {
                if ((snakeX.get(i1) == headX) && (snakeY.get(i1) == headY)) {
                    if (!((snakeX.get(snakeX.size() - 1) == headX) && (snakeY.get(snakeY.size() - 1) == headY))) {
                        isTouchItself = true;
                    }
                }
            }

            gameover = isTouchStone || isTouchItself;
            //adding
            snakeX.add(0, headX);
            snakeY.add(0, headY);
            //if eat the green stone, remove that stone, else just move the snake by removing the last index
            if (((snakeX.get(0) == greenX) && (snakeY.get(0) == greenY))) {
                greenX = -1;
                greenY = -1;
                setTitle("Program" + " : " + snakeX.size());
            } else {
                snakeX.remove(snakeX.size() - 1);
                snakeY.remove(snakeY.size() - 1);
            }

            //create new red & green stones by using random x & y positions
            if (greenX == -1) {
                //green
                int randomX, randomY;
                boolean check1 = false;
                boolean check2 = false;
                do {
                    randomX = RANDOM.nextInt(30);
                    randomY = RANDOM.nextInt(20);
                    for (int i = 0; i < stonesX.size(); i++) {
                        if (stonesX.get(i) == randomX && stonesY.get(i) == randomY) {
                            check1 = true;
                        }
                    }
                    for (int i = 0; i < snakeX.size(); i++) {
                        if ((snakeX.get(i) == randomX) && (snakeY.get(i) == randomY)) {
                            if (!((snakeX.get(snakeX.size() - 1) == randomX) && (snakeY.get(snakeY.size() - 1) == randomY))) {
                                check2 = true;
                            }
                        }
                    }
                } while (check2 || check1);
                greenX = randomX;
                greenY = randomY;

                //red
                int x1, y1;
                boolean check3 = false;
                boolean check4 = false;
                do {
                    x1 = RANDOM.nextInt(30);
                    y1 = RANDOM.nextInt(20);
                    for (int i = 0; i < stonesX.size(); i++) {
                        if (stonesX.get(i) == x1 && stonesY.get(i) == y1) {
                            check3 = true;
                        }
                    }
                    for (int i = 0; i < snakeX.size(); i++) {
                        if ((snakeX.get(i) == x1) && (snakeY.get(i) == y1)) {
                            if (!((snakeX.get(snakeX.size() - 1) == x1) && (snakeY.get(snakeY.size() - 1) == y1))) {
                                check4 = true;
                            }
                        }
                    }
                } while (check3 || check4 || greenX == x1 && greenY == y1);

                stonesX.add(x1);
                stonesY.add(y1);
            }

            canvas.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < snakeX.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(snakeX.get(i) * 25 + 1, snakeY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            g.setColor(Color.green);
            g.fill3DRect(greenX * 25 + 1, greenY * 25 + 1, 25 - 2, 25 - 2, true);
            for (int i = 0; i < stonesX.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(stonesX.get(i) * 25 + 1, stonesY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            if (gameover) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }
}