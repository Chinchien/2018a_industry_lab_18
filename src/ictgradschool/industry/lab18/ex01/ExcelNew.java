package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {

    public static void main(String[] args) {
        String outputData = "";
        int classSize = 550;
        ArrayList<String> firstNameList = new ArrayList<String>();
        ArrayList<String> surnameList = new ArrayList<String>();
        try {
            readFromFile("FirstNames.txt", firstNameList);
            readFromFile("Surnames.txt", surnameList);

            outputData = setOutput(outputData, classSize, firstNameList, surnameList);

            createOutputFile(outputData);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private static void createOutputFile(String output) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
        bw.write(output);
        bw.close();
    }

    private static String setOutput(String output, int classSize, ArrayList<String> firstNameList, ArrayList<String> surnameList) {
        for (int i = 1; i <= classSize; i++) {
            String student = "";
            student = setId(i, student);
            student = setFnameAndLname(firstNameList, surnameList, student);
            //Student Skill
            int randStudentSkill = (int) (Math.random() * 101);
            //Labs//////////////////////////
            student = setLabMarks(student, randStudentSkill);
            //Test/////////////////////////
            student = setTestMarkWithRange(student, randStudentSkill, 20, 65, 90);
            ///////////////Exam////////////
            student = setExamMark(student, randStudentSkill);
            //////////////////////////////////
            student += "\n";
            output += student;
        }
        return output;
    }

    private static String setExamMark(String student, int randStudentSkill) {
        if (randStudentSkill <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb <= 5) {
                student += ""; //DNS
            } else {
                student += (int) (Math.random() * 40); //[0,39]
            }
        } else if ((randStudentSkill > 7) && (randStudentSkill <= 20)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 60)) {
            student += ((int) (Math.random() * 20) + 50);//[50,69]
        } else if ((randStudentSkill > 60) && (randStudentSkill <= 90)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return student;
    }

    private static String setTestMarkWithRange(String student, int randStudentSkill, int i2, int i3, int i4) {
        if (randStudentSkill <= 5) {
            student += (int) (Math.random() * 40); //[0,39]
        } else if ((randStudentSkill > 5) && (randStudentSkill <= i2)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > i2) && (randStudentSkill <= i3)) {
            student += ((int) (Math.random() * 20) + 50); //[50,69]
        } else if ((randStudentSkill > i3) && (randStudentSkill <= i4)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        student += "\t";
        return student;
    }

    private static String setLabMarks(String student, int randStudentSkill) {
        int numLabs = 3;
        for (int j = 0; j < numLabs; j++) {
            student = setTestMarkWithRange(student, randStudentSkill, 15, 25, 65);
        }
        return student;
    }

    private static String setFnameAndLname(ArrayList<String> firstNameList, ArrayList<String> surnameList, String student) {
        int randFNIndex = (int) (Math.random() * firstNameList.size());
        int randSNIndex = (int) (Math.random() * surnameList.size());
        student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
        return student;
    }

    private static String setId(int i, String student) {
        if (i / 10 < 1) {
            student += "000" + i;
        } else if (i / 100 < 1) {
            student += "00" + i;
        } else if (i / 1000 < 1) {
            student += "0" + i;
        } else {
            student += i;
        }
        return student;
    }

    private static void readFromFile(String inputFile, ArrayList<String> inputList) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        while ((line = br.readLine()) != null) {
            inputList.add(line);
        }
        br.close();
    }

}